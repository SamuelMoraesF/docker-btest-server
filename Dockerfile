FROM alpine:latest AS builder
WORKDIR /tmp
RUN apk add --no-cache wget gcc musl-dev && \
    wget -qO- https://github.com/samm-git/btest-opensource/archive/master.zip | unzip - && \
    cd btest-opensource-master && gcc -o btest *.c -lpthread

FROM alpine:latest  
COPY --from=builder /tmp/btest-opensource-master/btest /usr/bin/btest
ENTRYPOINT ["/usr/bin/btest"]
